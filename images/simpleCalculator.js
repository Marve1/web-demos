
//paneme plussi tööle, lisa htmli click-element onclick"add()"" div id="opPlus" class = "clickable-element" onclick="add()"

function add() {
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let sum = parseInt(number1.value) + parseInt(number2.value);
    let result = document.querySelector("#result");
    result.innerHTML = sum;

}

function minus() {
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let sum = parseInt(number1.value) - parseInt(number2.value);
    let result = document.querySelector("#result");
    result.innerHTML = sum;
}

function multiply() {
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let sum = parseInt(number1.value) * parseInt(number2.value);
    let result = document.querySelector("#result");
    result.innerHTML = sum;
}

function divide() {
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let sum = parseInt(number1.value) / parseInt(number2.value);
    let result = document.querySelector("#result");
    result.innerHTML = sum;
}

function clearCalc() {  //PS! clear on juba mingi funktsioon ja ei tööta, teeb juba mingit väärtust
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    number1.value = "";
    number2.value = "";
    let result = document.querySelector("#result");
    result.innerHTML = "";
}